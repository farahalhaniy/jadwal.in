import random


def createdomain(daftarMatkul, term):
    domain = []
    for x in daftarMatkul:
        if daftarMatkul.get(x)[1] == term:
            domain.append(x)
    # (random.shuffle(domain))
    return domain


def createLibur(variable, hariLibur):
    for x in hariLibur:
        variable[x] = "LIBUR"
    return variable


def dfs(infoMatkul, graph, domains, variable, sks):
    ans = {}
    for x in domains:
        if sks > 0:
            kelas = 0
            listKelas = list(graph[x].keys())
            random.shuffle(listKelas)
            listHari = list(graph[x][listKelas[kelas]])

            n = 0
            while n < len(listHari) and kelas < len(graph[x].keys()):
                if variable[listHari[n]] == "LIBUR":
                    kelas += 1
                    if kelas < len(listKelas):
                        listHari = list(graph[x][listKelas[kelas]])
                    n = 0
                else:
                    if not variable[listHari[n]]:
                        variable[listHari[n]].append(graph[x][listKelas[kelas]][listHari[n]])
                    else:
                        waktu_domain = graph[x][listKelas[kelas]][listHari[n]].split(' - ')
                        for z in variable[listHari[n]]:
                            if waktu_domain[0] == z.split(' - ')[0]:
                                kelas = + 1
                                if kelas < len(listKelas):
                                    listHari = list(graph[x][listKelas[kelas]])
                            else:
                                variable[listHari[n]].append(graph[x][listKelas[kelas]][listHari[n]])
                            break

                    n += 1

            sks -= infoMatkul[x][0]
            if kelas < len(listKelas):
                if sks >= 0 and kelas < len(graph[x][listKelas[kelas]]):
                    ans[x] = graph[x][listKelas[kelas]]
                else:
                    print(ans)
                    return ans


        else:
            print(ans)
            return ans
    print(ans)
    return ans


def main():
    daftarMatkul = {
        # TERM 1
        "DDP1": [4, 1, 2], "MATDIS1": [3, 1, 2], "MPKTB": [6, 1, 2], "MPKING": [3, 1, 2], "MATDAS1": [3, 1, 2],
        # TERM 2
        "PSD": [4, 2, 2], "DDP2": [4, 2, 2], "MATDAS2": [3, 2, 2], "MATDIS2": [3, 2, 2], "MPKTA": [6, 2, 2],
        # TERM 3
        "SDA": [4, 3, 2], "ALIN": [3, 3, 2], "POK": [4, 3, 2], "PPW": [4, 3, 2], "FISDAS": [3, 3, 2],
        # TERM 4
        "STATPROB": [3, 4, 2], "OS": [4, 4, 2], "BASDAT": [4, 4, 2], "ADVPROG": [4, 4, 2], "TBA": [4, 4, 2],
        # TERM 5
        "RPL": [3, 5, 2], "SYSPROG": [3, 5, 2], "SC": [4, 5, 2], "JARKOM": [4, 5, 2],
        # TERM 6
        "MPPI": [3, 6, 2], "PPL": [6, 6, 2], "DSA": [3, 6, 2], "ANUM": [3, 6, 2],
        # TERM 7
        "KP": [3, 7, 2], "DAA": [4, 7, 2],
        # TERM 8
        "KOMAS": [3, 8, 2],

    }
    daftarJadwal = {
        # TERM 1
        "DDP1": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                 'B': {"Selasa": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Kamis": '13.00 - 14.40'},
                 },
        "MPKTB": {'A': {"Senin": '10.00 - 10.40', "Selasa": '11.00 - 12.40', "Jumat": '08.00 - 09.40'},
                  'B': {"Rabu": '08.00 - 09.40', "Kamis": '08.00 - 09.40', "Jumat": '10.00 - 10.40'},
                  },
        "MPKING": {'A': {"Senin": '11.00 - 10.40', "Selasa": '13.00 - 14.40'},
                   'B': {"Kamis": '10.00 - 10.40', "Rabu": '11.00 - 12.40'},
                   },
        "MATDIS1": {'A': {"Senin": '13.00 - 14.40', "Selasa": '15.00 - 16.40'},
                    'B': {"Selasa": '17.00 - 18.40', "Rabu": '13.00 - 14.40'},
                    },
        "MATDAS1": {'A': {"Kamis": '11.00 - 12.40', "Jumat": '11.00 - 12.40'},
                    'B': {"Senin": '17.00 - 18.40', "Kamis": '13.00 - 14.40'},
                    },

        # TERM 2
        "DDP2": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                 'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                 },
        "MATDIS2": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                    'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                    },
        "PSD": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '10.00 - 10.40', "Jumat": '14.00 - 15.40'},
                'B': {"Senin": '10.00 - 10.40', "Rabu": '15.00 - 16.40', "Jumat": '17.00 - 18.40'},
                },
        "MATDAS2": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                    'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                    },
        "MPKTA": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                  'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                  },

        # TERM 3
        "SDA": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                },
        "ALIN": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                 'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                 },
        "POK": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                },
        "PPW": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                },
        "FISDAS": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                   'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                   },

        # TERM 4
        "STATPROB": {'A': {"Selasa": '08.00 - 09.40', "Kamis": '10.00 - 09.50'},
                     'B': {"Senin": '10.00 - 10.40', "Kamis": '11.00 - 11.50'},
                     },
        "OS": {'A': {"Selasa": '13.00 - 14.40', "Kamis": '13.00 - 14.40', "Jumat": '14.00 - 15.40'},
               'B': {"Selasa": '10.00 - 10.40', "Kamis": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
               },
        "BASDAT": {'A': {"Selasa": '08.00 - 09.40', "Jumat": '17.00 - 18.40'},
                   'B': {"Rabu": '10.00 - 10.40', "Jumat": '08.00 - 09.40'},
                   },
        "ADVPROG": {'A': {"Senin": '13.00 - 14.40', "Rabu": '14.00 - 15.40', "Jumat": '10.00 - 10.40'},
                    'B': {"Senin": '10.00 - 10.40', "Rabu": '08.00 - 09.40', "Jumat": '10.00 - 10.40'},
                    },
        "TBA": {'A': {"Senin": '10.00 - 10.40', "Rabu": '08.00 - 09.40'},
                'B': {"Senin": '13.00 - 14.40', "Rabu": '10.00 - 10.40'},
                },

        # TERM 5
        "RPL": {'A': {"Selasa": '08.00 - 09.40', "Kamis": '10.00 - 09.50'},
                'B': {"Selasa": '10.00 - 10.40', "Kamis": '10.00 - 10.40'},
                },
        "SYSPROG": {'A': {"Senin": '13.00 - 14.40', "Jumat": '10.00 - 10.40'},
                    'B': {"Senin": '08.00 - 09.40', "Jumat": '08.00 - 09.40'},
                    },
        "SC": {'A': {"Selasa": '10.00 - 10.40', "Kamis": '10.00 - 10.40'},
               'B': {"Rabu": '14.00 - 15.40', "Jumat": '13.00 - 14.40'},
               },
        "JARKOM": {'A': {"Senin": '08.00 - 09.40', "Rabu": '08.00 - 09.40'},
                   'B': {"Senin": '10.00 - 10.40', "Rabu": '10.00 - 10.40'},
                   },

        # TERM 6
        "MPPI": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                 'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                 },
        "PPL": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                },
        "DSA": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                },
        "ANUM": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                 'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                 },

        # TERM 7
        "KP": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
               'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
               },
        "DAA": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                },

        # TERM 8
        "KOMAS": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
                  'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                  },
    }

    hari = {'Senin': [], 'Selasa': [], 'Rabu': [], 'Kamis': [], 'Jumat': []}
    hariLibur = []
    sks = 19
    term = 5
    domain = createdomain(daftarMatkul, term)
    var = createLibur(hari, hariLibur)
    dfs(daftarMatkul, daftarJadwal, domain, var, sks)


if __name__ == '__main__':
    main()
