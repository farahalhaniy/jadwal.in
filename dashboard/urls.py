from django.urls import path
from .views import *

urlpatterns = [
    path('dashboard/', index, name='index'),
    path('form/', index_form, name='form'),
    path('generate/', generate, name='generate'),
    path('output/', hasil, name='hasil'), ]
