from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .utils.logic import *
import ast

# Create your views here.

# Data
response = {}
hari = {
    'Senin': [], 'Selasa': [], 'Rabu': [], 'Kamis': [], 'Jumat': []
}
daftarMatkul = {
    # TERM 1
    # Total SKS: 19
    "DDP1": [4, 1, 2], "MATDIS1": [3, 1, 2], "MPKTB": [6, 1, 2], "MPKING": [3, 1, 2], "MATDAS1": [3, 1, 2],
    # TERM 2
    # Total SKS: 20
    "PSD": [4, 2, 2], "DDP2": [4, 2, 2], "MATDAS2": [3, 2, 2], "MATDIS2": [3, 2, 2], "MPKTA": [6, 2, 2],
    # TERM 3
    "SDA": [4, 3, 2], "ALIN": [3, 3, 2], "POK": [4, 3, 2], "PPW": [4, 3, 2], "FISDAS1": [3, 3, 2],
    # TERM 4
    "STATPROB": [3, 4, 2], "OS": [4, 4, 2], "BASDAT": [4, 4, 2], "ADVPROG": [4, 4, 2], "TBA": [4, 4, 2],
    # TERM 5
    "RPL": [3, 5, 2], "SYSPROG": [3, 5, 2], "SC": [4, 5, 2], "JARKOM": [4, 5, 2],
    # TERM 6
    "MPPI": [3, 6, 2], "PPL": [6, 6, 2], "DSA": [3, 6, 2], "ANUM": [3, 6, 2],
    # TERM 7
    "KP": [3, 7, 2], "DAA": [4, 7, 2],
    # TERM 8
    "KOMAS": [3, 8, 2],

}
daftarJadwal = {
    # TERM 1
    "DDP1": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
             'B': {"Selasa": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Kamis": '13.00 - 14.40'},
             },
    "MPKTB": {'A': {"Senin": '10.00 - 10.40', "Selasa": '11.00 - 12.40', "Jumat": '08.00 - 09.40'},
              'B': {"Rabu": '08.00 - 09.40', "Kamis": '08.00 - 09.40', "Jumat": '10.00 - 10.40'},
              },
    "MPKING": {'A': {"Senin": '11.00 - 10.40', "Selasa": '13.00 - 14.40'},
               'B': {"Kamis": '10.00 - 10.40', "Rabu": '11.00 - 12.40'},
               },
    "MATDIS1": {'A': {"Senin": '13.00 - 14.40', "Selasa": '15.00 - 16.40'},
                'B': {"Selasa": '17.00 - 18.40', "Rabu": '13.00 - 14.40'},
                },
    "MATDAS1": {'A': {"Kamis": '11.00 - 12.40', "Jumat": '11.00 - 12.40'},
                'B': {"Senin": '17.00 - 18.40', "Kamis": '13.00 - 14.40'},
                },

    # TERM 2
    "DDP2": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
             'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
             },
    "MATDIS2": {'A': {"Senin": '13.00 - 14.40', "Kamis": '08.00 - 09.40', "Jumat": '17.00 - 18.40'},
                'B': {"Senin": '08.00 - 09.40', "Kamis": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
                },
    "PSD": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '10.00 - 10.40', "Jumat": '14.00 - 15.40'},
            'B': {"Senin": '10.00 - 10.40', "Rabu": '15.00 - 16.40', "Jumat": '15.00 - 16.40'},
            },
    "MATDAS2": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
                'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
                },
    "MPKTA": {'A': {"Rabu": '10.00 - 10.40', "Kamis": '14.00 - 14.50'},
              'B': {"Selasa": '13.00 - 14.40', "Kamis": '10.00 - 10.50'},
              },

    # TERM 3
    "SDA": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
            'B': {"Selasa": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Kamis": '13.00 - 14.40'},
            },
    "ALIN": {'A': {"Senin": '10.00 - 10.40', "Selasa": '11.00 - 12.40'},
             'B': {"Rabu": '08.00 - 09.40', "Kamis": '08.00 - 09.40'},
             },
    "POK": {'A': {"Senin": '11.00 - 10.40', "Selasa": '13.00 - 14.40', "Jumat": '08.00 - 09.40'},
            'B': {"Kamis": '10.00 - 10.40', "Rabu": '11.00 - 12.40', "Jumat": '10.00 - 10.40'},
            },
    "PPW": {'A': {"Senin": '13.00 - 14.40', "Selasa": '15.00 - 16.40', "Jumat": '13.00 - 14.40'},
            'B': {"Selasa": '17.00 - 18.40', "Rabu": '13.00 - 14.40', "Jumat": '15.00 - 16.40'},
            },
    "FISDAS1": {'A': {"Kamis": '11.00 - 12.40', "Jumat": '11.00 - 12.40'},
                'B': {"Senin": '17.00 - 18.40', "Kamis": '13.00 - 14.40'},
                },

    # TERM 4
    "STATPROB": {'A': {"Selasa": '08.00 - 09.40', "Kamis": '10.00 - 10.40'},
                 'B': {"Senin": '10.00 - 10.40', "Kamis": '11.00 - 11.50'},
                 },
    "OS": {'A': {"Selasa": '13.00 - 14.40', "Kamis": '13.00 - 14.40'},
           'B': {"Selasa": '10.00 - 10.40', "Kamis": '10.00 - 10.40'},
           },
    "BASDAT": {'A': {"Selasa": '08.00 - 09.40', "Rabu": '14.00 - 15.40', "Jumat": '17.00 - 18.40'},
               'B': {"Senin": '17.00 - 18.40', "Rabu": '10.00 - 10.40', "Jumat": '08.00 - 09.40'},
               },
    "ADVPROG": {'A': {"Senin": '13.00 - 14.40', "Rabu": '14.00 - 15.40', "Jumat": '10.00 - 10.40'},
                'B': {"Senin": '10.00 - 10.40', "Rabu": '08.00 - 09.40', "Jumat": '10.00 - 10.40'},
                },
    "TBA": {'A': {"Senin": '17.00 - 18.40', "Rabu": '08.00 - 09.40'},
            'B': {"Senin": '13.00 - 14.40', "Rabu": '10.00 - 10.40'},
            },

    # TERM 5
    "RPL": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40'},
            'B': {"Selasa": '08.00 - 09.40', "Rabu": '10.00 - 10.40'},
            },
    "JARKOM": {'A': {"Senin": '10.00 - 10.40', "Selasa": '11.00 - 12.40'},
               'B': {"Rabu": '08.00 - 09.40', "Kamis": '08.00 - 09.40'},
               },
    "SC": {'A': {"Senin": '11.00 - 12.40', "Selasa": '13.00 - 14.40'},
           'B': {"Kamis": '10.00 - 10.40', "Rabu": '11.00 - 12.40'},
           },
    "SYSPROG": {'A': {"Senin": '13.00 - 14.40', "Selasa": '15.00 - 16.40', "Kamis": '11.00 - 12.40'},
                'B': {"Selasa": '17.00 - 18.40', "Rabu": '13.00 - 14.40', "Jumat": '11.00 - 12.40'}},

    # TERM 6
    "MPPI": {'A': {"Selasa": '08.00 - 09.40', "Kamis": '10.00 - 10.40'},
             'B': {"Senin": '10.00 - 10.40', "Kamis": '11.00 - 12.40'},
             },
    "DSA": {'A': {"Selasa": '13.00 - 14.40', "Kamis": '13.00 - 14.40', "Jumat": '14.00 - 15.40'},
            'B': {"Selasa": '10.00 - 10.40', "Kamis": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
            },
    "ANUM": {'A': {"Selasa": '08.00 - 09.40', "Jumat": '17.00 - 18.40'},
             'B': {"Rabu": '10.00 - 10.40', "Jumat": '08.00 - 09.40'},

             },
    "PPL": {'A': {"Senin": '13.00 - 14.40', "Rabu": '14.00 - 15.40', "Jumat": '10.00 - 10.40'},
            'B': {"Senin": '15.00 - 10.40', "Rabu": '08.00 - 09.40', "Jumat": '10.00 - 10.40'},
            },

    # TERM 7
    "KP": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
           'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
           },
    "DAA": {'A': {"Senin": '10.00 - 10.40', "Selasa": '08.00 - 09.40', "Jumat": '15.00 - 16.40'},
            'B': {"Senin": '10.00 - 10.40', "Rabu": '08.00 - 09.40', "Jumat": '15.00 - 16.40'},
            },

    # TERM 8
    "KOMAS": {'A': {"Senin": '08.00 - 09.40', "Selasa": '10.00 - 10.40', "Jumat": '17.00 - 18.40'},
              'B': {"Senin": '08.00 - 09.40', "Rabu": '10.00 - 10.40', "Jumat": '13.00 - 14.40'},
              },
}


def index(request):
    template = 'index.html'
    return render(request, template, {})


def index_form(request):
    template = 'form.html'
    return render(request, template, {})


@csrf_exempt
def generate(request):
    if request.method == 'POST':
        data = {}
        nama = request.POST.get('nama')
        npm = request.POST.get('npm')
        term = request.POST.get('term')
        sks = request.POST.get('sks')
        libur = request.POST.get('libur')
        nama = str(nama)
        npm = str(npm)
        term = int(term)
        libur = str(libur)
        sks = int(sks)
        domain = createdomain(daftarMatkul, term)
        hariLibur = libur.split(',')
        var = createLibur(hari, hariLibur)
        ans = dfs(daftarMatkul, daftarJadwal, domain, var, sks)
        response['libur'] = libur
        print(response['libur'])
        response['nama'] = nama
        response['npm'] = npm
        data['ans'] = ans
        response['sks'] = sks
        response['term'] = term
        print(response)
        f = open("data.txt", "w+")
        f.write(str(response))
        f.close()
        f = open("jadwal.txt", "w+")
        f.write(str())
        f.write(str(data))
        f.close()

    return render(request, 'form.html', response)


def hasil(request):
    output = {}
    template = 'output.html'
    f = open("data.txt", "r")
    if f.mode == 'r':
        output['data'] = ast.literal_eval(f.read())
    f = open("jadwal.txt", "r")
    if f.mode == 'r':
        output['jadwal'] = ast.literal_eval(f.read())

    return render(request, template, output)
