**PROJECT NAME: JADWAL.IN**

**Artificial Intelligence (CSP)**

[Computer Science UI SCHEDULER BOT]

We are using DFS algorithm for solve the problem.

Variables: Course day and time

Domains: Courses

Constraints: No blocking course, holiday, and credits

Input: Name, Student ID, Credits, Term, and Holiday

Output: Your Course Plan

**ALOHA! MEMBER:**

- Farah Alhaniy Efendi		(1606821886)
- Metta Anantha Pindika		(1606876046)
- Muhammad Jihad Rinaldi	(1606883392)

**CLASS: A**

**MENTOR: Fersandi**

<br>

**This is an Open Source Project**
1. Clone this repository to your local (`git clone https://gitlab.com/farahalhaniy/jadwal.in.git`)
2. Create and activate the venv python (`python -m venv env`)
3. Install the dependency (`$pip install -r requirements.txt`)
4. Run it local using (`$python manage.py runserver`)
5. Fill the form and you can look the result in the **JADWALMU!** tab